import { parseISO, format } from 'date-fns';

function translateMonth(str) {
    const month_fr = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"]
    const month_en = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    let result = str;
    month_en.map((elt, i) => {
        if (str.includes(elt)) {
            result = str.replace(elt, month_fr[i]);
        }
    });
    return result;
}

export default function Date({ dateString }) {
    const date = parseISO(dateString);
    return <time dateTime={dateString}>{ translateMonth(format(date, 'd LLLL, yyyy')) }</time>
}