import Head from 'next/head';
import Navbar from './navbar';
import Image from 'next/image';
import styles from './layout.module.css';
import utilStyles from '../styles/utils.module.css';
import Link from 'next/link';
import {user, root_path, get_cmd} from '../lib/cmd'
import Cmdline from './cmd';
import { Endline } from './cmd';

const name = 'Victor Lohézic';

export const siteTitle = 'Next.js Sample Website';

export default function Layout({ children, next_path, home }) {
  next_path = next_path ? next_path : "";
  const path = root_path + next_path;
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <meta
          property="og:image"
          content={`https://og-image.vercel.app/${encodeURI(
            siteTitle,
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header className={styles.header}>
        {home ? (
          <>
            <Navbar />
            {/* <h1 className={utilStyles.heading2Xl}>{name}</h1> */}
          </>
        ) : (
          <>
            <Navbar next_path={next_path} />
          </>
        )}
      </header>
      < br/>
      < br/>
      <main>
        <span className={ styles.firstcmd }> {next_path!=="blog/posts" ? <Cmdline next_path={next_path} /> : <></>}</span>
        {children}
        </main>
      {!home && (
          <Endline next_path={next_path} />
      )}
    </div>
  );
}
