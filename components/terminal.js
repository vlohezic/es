export default function Terminal() {
    return (
        <div>
            <p>
                Bienvenido a mi sitio web. <br />
                En el directorio blog, comparto proyectos que he realizado, artículos sobre tecnologías que he descubierto. 
                Puede verse como un cuaderno. En el directorio cv puede obtener más informaciones sobre mí. 
                Por último, el apartado contacto le permite ponerse en contacto conmigo o seguir mis actividades.
            </p>
            <p>
                Es una versión en español de mi <a href="/">sitio web</a>. El español no es mi lengua nativa.
            </p>
            <p>
                El diseño de este sitio es el de una terminal. Esta referencia informatica aligera el diseño. Asimismo, convertí les imagenes para el blog 
                en Arte ASCII. El peso con respecto a una imagen es más bajo. Para crear este sitio web, he utilizado el aspecto sistema de plantillas web de Next js. 
                No necesista un lenguaje para el servidor. El aspecto estático permite tener una carga más rápida de las páginas. 
                Además de los desempeños, es también pasos de ecodiseño web. He intentado a lo sumo seguir las prácticas de este libro : <a href="https://www.eyrolles.com/Informatique/Livre/ecoconception-web-les-115-bonnes-pratiques-9782416006272/" target="__blanck">Ecoconception web : les 115 bonnes pratiques</a>. 
                Sin embargo, he realizado algunas animaciones en CSS porque el ecodiseño es un equilibrio entre ecología y la experiencia de usuario. 
                Estar al tanto de las metas de nuestro siglo, <a href="https://vimeo.com/32564879" target="__blanck">seamos el colibrí que está apagando el bosque</a>. 
            </p>
            
        </div>
    );
}