import Layout from "../components/layout";
import Link from 'next/link';
import Date from '../components/date';
import { getSortedPostsData } from '../lib/posts';
import utilStyles from '../styles/utils.module.css';
import blogStyles from '../styles/blog.module.css';

export async function getStaticProps() {
    const allPostsData = getSortedPostsData();
    return {
        props: {
            allPostsData,
        }
    };
}


export default function Blog({ allPostsData }) {
    // let htmlContent = fs.readFileSync("../thumbnails/popartech.html", 'utf8');
    return (
        <Layout next_path="blog">
            <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
                <div id={blogStyles.blog}>
                    {allPostsData.map(({ id, title, thumbnail }) => (
                        <div className={utilStyles.listItem} key={id}>
                            <br />
                            <Link href={`/es/posts/${id}`}>
                                <a className={blogStyles.title} >
                                    <div dangerouslySetInnerHTML={{ __html: thumbnail }} className={blogStyles.thumbnail} />
                                    {title}
                                </a>
                            </Link>
                        </div>
                    ))}
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </section>
        </Layout>
    );
}
