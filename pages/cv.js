import Layout from "../components/layout";
import Cmdline from "../components/cmd";
import asciiStyles from "../styles/ascii.module.css";
import cvStyles from '../styles/cv.module.css';
import Link from "next/link";

export default function Cv() {
    const next_path = "cv";
    return (
        <Layout next_path={next_path}>
            <pre className={asciiStyles.photo_ascii}>
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ....................................................................................................<br />
                ...............................................!7?JJYYYJJJ?7!.......................................<br />
                .........................................!7JY5PGGPPGGGGP55PP55YJ7!..................................<br />
                ......................................7Y5PPGBBBGGBBBGGGPPGGGGGGGGP5Y7...............................<br />
                ....................................?5PGGBBBGGGGBBBBBBBBB########BGGP5J.............................<br />
                ..................................7J5PPGBBBGBBBBGGGGGGGBBBBBBB##BBBGGPPY............................<br />
                .................................75PPGBBBBBB#BGGGBBBB#B######BBBBBBBGGPPY!..........................<br />
                ................................7YGGB##BBBB#BBBBBBBBBGGGGGGGGGBBGGGGGGGP55?.........................<br />
                ................................YB##BGP555YYYYYYYY555YYJJJJJJJYYY555PGBBP5557.......................<br />
                ...............................JPBBG5J????777777777??77777777777????JY5GGPPP5!......................<br />
                ..............................J5PGPY???7777777777777777777777777?7?????Y5GPP5J......................<br />
                .............................755PPJ?????7777777777!!!!777777777????????JJY5P5J......................<br />
                .............................?55YJ??????777777777777777777777777??????JJJYYY5?......................<br />
                .............................7YYJ?????????77777777777777777777??????JJJJYYYYY?......................<br />
                ..............................JJJ??????????7777777777777777777?????JJJJYYYYYY7......................<br />
                ..............................7J????????7777777777777777777777??????JJJYYJJY57......................<br />
                ...............................J????????777777!!!!!!!!!!77777????JJJJJJJJJJJY!......................<br />
                ...............................7?7??JY55555JJ??7777777777??JY55PGBGGPGG5YJ?JJ.......................<br />
                ...............................!77J5PPPPPGBBBGPYJ??77???J5PB###BBGPPP5555Y?J?!......................<br />
                ..............................??77?JJY55GBB#GGPP5J?77?JYPGGGGPG#B#BPGG5YYYJJJYJ.....................<br />
                ..............................J?77??YP5JPB#GJJYYYJ??7?J55555J?JGBG5YPP5YYYJ?Y5Y.....................<br />
                ..............................??77??JJYJJYYYJJJJYJ???JY55YYYJJJYYYYYYYJJJJJ?JYJ.....................<br />
                ...............................777?????JJ?????JJJJ???JYYYYYJJJJJJ???JJJJJJJJYJ......................<br />
                ...............................!777?????????????JJ???JYYYYYJ????????JJJJJJJJY7......................<br />
                ................................!777777777777??JJ?????JYY5YJ?????????JJJJJJJJ!......................<br />
                ................................!7777777777777????777?JJYY5J?77??????JJJJJJYJ!......................<br />
                ..................................777777777777?JJJ?77?J55Y5Y??7?????JJJJJJ?7!.......................<br />
                ..................................!7777777777??JYYJJYYPPP5YJJ?????JJJJJJJJ?!!.......................<br />
                ...................................7??77???????JJJJJYYYYYYYJJJJJJJJJJJJJJJ?!........................<br />
                ...................................7??????JJJ??????J???JJJYYYY55YYYYYYYYYJ?!........................<br />
                ....................................?????JYYYYYYYYYYYY5555PPPPP5YYYYYYYYYJ7!........................<br />
                ....................................!????????JYYYYJJJYYYY5555YYJJYYYYYYYY?!.........................<br />
                .....................................!??????????JJJJYYYYYYYYJJJJYYYYYYYY?!!.........................<br />
                ......................................!?JJJ?????????JJJ???JJJJJJYYYY55J7!!..........................<br />
                ........................................JYJJJ?????????????JJJJJYY55PPY77!!..........................<br />
                ........................................?JYYJJJJ????7??JJJJYYYY5PGPP5Y77!!..........................<br />
                .....................................!!7???JY5YYJJ????JJJY55PPGGP55YYY?7!!..........................<br />
                ..................................!7YPJJ?????JY55555555PPGGGGPP5YYYYYYYJ777!........................<br />
                ............................!5PGGBBPP5JJ?????????JJJJJYYYYYYYYYYYYYYY55YY5GG?77!....................<br />
                .....................7JY5PGG#&####BPPPJ?J???777?????777??JJYYYYJYYYY55555Y5BBB##B5?!!...............<br />
                ..............7777?YB#B#&&#&##B####GPG5J???777777??????JJJJJJJJJJYYY555555G###&&&&&#BG5Y?7!.........<br />
                ........?JY5GB######BB#&&#####B##&#BGGG5J??777777777??????J????JJYYY55555GB##&&&&&&&&&&&##BG55J!....<br />
                ......JGB#####BBBBBB##&&#########&##BGGGPJ?7777777777777???????JJYYY555PG##&&&&&&&&&&&&&######BG5?!.<br />
                .?Y5PGB#####BBBB#####&&##########&&###BGGG5J?77777777777777????JJYYY55PG#&&&&&&&&&&&&&&&########BBGP<br />
                GGGGBBB###########&&&&###########&&&###BBGGGPYJ?77777777777???JY5PPPGB##&&&&&&&&&&&&&&&&&########BB#<br />
                GGGGGB##########&&&&&############&&&&##&##BBBBBGGPP55555555PGGBB####&&&@&&&&&&&&&&&&&&&&&&&#######BB<br />
                BBBBBB########&&&##&&#######&####&&&&&#&&########B###############&&&&&&&&&&&&&&&&&&&&&&&&&####&####B<br />
                B##BB######&#&&&###########&#####&&&&&&#&&&###&##########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&######&###<br />
                B##BB###&#&&###############&#####&&####&##&&###&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&######&###<br />
                ##BBB##&&&#&#############B#&#####&&########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&###########<br />
                ####B##&&&###############B#&######&#########&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&#####&&#&##<br />
                ####B#&&&####################################&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&#&&&&&&###########<br />
            </pre>
            <Cmdline next_path={next_path} cmd="echo $nombre" />
            Victor Lohézic
            <Cmdline next_path={next_path} cmd="cat formación.txt" />
            <div className={cvStyles.grid2col}>
                <div><b>2021-2024</b></div><div> Estudiante de segundo año de Informática en la Escuela Nacional Superior de Ingeniería ENSEIRB-MATMECA, de Burdeos (Francia)</div>
                <div><b>2022</b></div><div>  Statut National Étudiant-Entrepreneur Estatuto para estudiar y ser empresario</div>
                <div><b>2019-2021</b></div><div>  Formación Superior intensiva en Matemáticas y Física como preparación al examen de ingreso a las Escuelas Nacionales Superiores de Ingeniería Francesas.</div>
                <div><b>2019</b></div><div>  Bachillerato Científico Instituto Colbert, Lorient (Francia)</div>
            </div>
            <br/>
            <br/>
            <Cmdline next_path={next_path} cmd="cat pasiones.txt" />
            <div className={cvStyles.grid2col}>
                <div><b>Eirlab </b><br /><span className={cvStyles.description}>FabLab en el seno del ENSEIRB-MATMECA en Burdeos</span></div><div>Corte con láser</div>
                <div><b>Popartech </b><br /><span className={cvStyles.description}>asociación de makers en el seno de la secundaria Anita-Conti en Lorient </span></div><div>Creé un sitio web y redacté artículos</div>
                <div><b>Más grande grupo de rock del mundo </b><br /><span className={cvStyles.description}>Lorient</span></div><div>Tuba</div>
            </div>
            <br />
            <Cmdline next_path={next_path} cmd="ls experiencia_y_proyectos_profesionales" />
            <div className={cvStyles.flexcol}>
                <Link href={"/posts/robotont"}>
                    <div className={cvStyles.box}>Armé y programmé un robot móvil empleando ROS denominado Robotont</div>
                </Link>
            </div>
            <Cmdline next_path={next_path} cmd="ls proyectos" />
            <div className={cvStyles.flexcol}>
                <Link href={"https://flippanim.com/"}>
                    <div className={cvStyles.box}>Desarollo con Django https://flippanim.com</div>
                </Link>
            </div>
            <div className={cvStyles.flexcol}>
                <Link href={"https://zestedesavoir.com/"}>
                    <div className={cvStyles.box}>Estoy jefe de proyecto estudiantil que desarollo el sitio ZesteDeSavoir, además creé un software utilizando PyQt para facilitar el envío de emails con informe</div>
                </Link>
            </div>
            <div className={cvStyles.flexcol}>
                <Link href={"/posts/wator"}>
                    <div className={cvStyles.box}>Modelé en Python un autómata celular para estudiar del impacto del hombre sobre  sobre las poblaciones del atún rojo y de la orca con el fin de notar las ventajas de una pesca sotenible y de las reservas naturales</div>
                </Link>
            </div>
            <div className={cvStyles.flexcol}>
                <Link href={"/posts/popartech"}>
                    <div className={cvStyles.box}>Creé un sitio web para la asociación PopArTech con un sistema de plantillas web </div>
                </Link>
            </div>
            <Cmdline next_path={next_path} cmd="cat idiomas_y_softwares.txt" />
            <br/>
            <div className={cvStyles.flexcol}>
                <table className={cvStyles.skills}>
                    <tr>
                        <td className={cvStyles.skillsTd}>C++</td>
                        <td className={cvStyles.skillsTd}>Python</td>
                        <td className={cvStyles.skillsTd}>Java</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>HTML</td>
                        <td className={cvStyles.skillsTd}>CSS</td>
                        <td className={cvStyles.skillsTd}>JS</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>React</td>
                        <td className={cvStyles.skillsTd}>Django</td>
                        <td className={cvStyles.skillsTd}>NextJS</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>C</td>
                        <td className={cvStyles.skillsTd}>git / GitLab / GitHub</td>
                        <td className={cvStyles.skillsTd}>ROS</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>Oracle</td>
                        <td className={cvStyles.skillsTd}>PostgreSQL</td>
                        <td className={cvStyles.skillsTd}>MySQL</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>Inckscape</td>
                        <td className={cvStyles.skillsTd}>Maya/OpenToonz</td>
                        <td className={cvStyles.skillsTd}>PyQT</td>
                    </tr>
                    <tr>
                        <td className={cvStyles.skillsTd}>TOEIC 885</td>
                        <td className={cvStyles.skillsTd}>Reading C1</td>
                        <td className={cvStyles.skillsTd}>Writing B2</td>
                    </tr>
                </table>
            </div>
            <br/>
        </Layout>
    );
}
