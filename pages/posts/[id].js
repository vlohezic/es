import Head from 'next/head';
import Layout from "../../components/layout";
import { getAllPostIds, getPostData } from '../../lib/posts';
import Date from '../../components/date';
import utilStyles from '../../styles/utils.module.css';

export default function Post({ postData }) {
  const path = postData.title.replace(" ", "-") + ".md";
  return (
    <Layout next_path="blog/posts" >
      <Head>
        <title>{postData.title}</title>
      </Head>
      <article>
        <span className="user">vlohezic@gitlab</span>:<span className="path">~/blog/posts</span>$ cat { path } <br/>
        <h1 className={utilStyles.headingXl}>{postData.title}</h1>
        <div className={utilStyles.lightText}>
          <Date dateString={postData.date} />
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </article>
    </Layout>
  );
}



export async function getStaticPaths() {
    const paths = getAllPostIds();
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id);
    return {
        props: {
            postData,
        },
    };
}


