import Layout from "../components/layout";
import Cmdline from "../components/cmd";
import asciiStyles from "../styles/ascii.module.css";
import cvStyles from '../styles/cv.module.css';
import Link from "next/link";

export default function Cursos() {
    const next_path = "cursos";
    return (
        <Layout next_path={next_path}>
            <br/>
            <br/>
            <div className={cvStyles.grid2col}>
                <div><b>Protocolo de control de transmisión</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>TCP/IP, ICMP, ARP</div>
                <div><b>Sistema de gestión de bases de datos</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Adquirir una competencia en un ámbito de bases de datos</div>
                <div><b>Iniciación a la Información y comunicación cuántica</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Algoritmias cuántica : algoritmos de Deutsch y de Simon, el algoritmo de Shor</div>
                <div><b>Programación C++</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Presenta la basas de la programación C++, especialmente las herramientas y las técnicas con respecto a la gestión de memoria</div>
                <div><b>Programación de sistemas operativos</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Presenta los conceptos y las técnicas para programar en un entorno de trabajo digita POSIX</div>
                <div><b>Programación orientada a objetos</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Introducción a la programación orientada a objetos con el lenguaje Java</div>
                <div><b>Ingeniería de software</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Herramientas para el desarrollo de software, el desarrollo ágil de software</div>
                <div><b>Compilación</b><br /><span className={cvStyles.description}>Semestre 7</span></div><div>Presentación de las técnicas y las herramientas para la compilación de los lenguajes de programmation</div>
                <div><b>Introducción a las redes de computadoras</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Introducción a las redes de computadoras con los modelos OSI y TCP/IP</div>
                <div><b>Programación imperativa 2 y desarrollo de software</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Aprofundización de los mecanismos de gestión de memoria, de la compilación y apropiación a la herramientas del desarrollo de software</div>
                <div><b>Programación funcional</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Estudio un Paradigma con JavaScript</div>
                <div><b>Investigación de operaciones</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Optimización matemática</div>
                <div><b>Algorítmica Numérica</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Algoritmia para la algoritmia de problemas digitales</div>
                <div><b>Autómata finito y aplicación</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Autómata finito, lenguajes regulares, expresiones regulares, equivalencia de estos formulismos, no determinista, minimización, lema del bombeo</div>
                <div><b>Teoría de grafos</b><br /><span className={cvStyles.description}>Semestre 6</span></div><div>Introducción de grafos</div>
                <div><b>Programación imperativa 1</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Aprender las bases de la programación imperativa para el estudi de la sintaxis y la sintaxis del languaje C</div>
                <div><b>Arquitectura de computadoras</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Arquitectura de computadoras, para los procesadores généralistas</div>
                <div><b>Shell</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Programación shell para automatizar las tareas de administración de las sistemas.</div>
                <div><b>Lógica y evidencia</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Corrección de un algoritmo y la completa ejecución de un algoritmo </div>
                <div><b>Teoría de la probabilidad y estadística</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div> Probabilidad discreta y continua, función de distribución de probabilidad y función de densidad de probabilidad</div>
                <div><b>Tratamiento de la información</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div><a href="https://es.wikipedia.org/wiki/An%C3%A1lisis_de_componentes_principales">ACP</a>, <a href="https://es.wikipedia.org/wiki/An%C3%A1lisis_de_correspondencias">análisis de correspondencias</a>, introducción al eprendizaje automático</div>
                <div><b>Topología arbórea</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Árboles y tipo de dato abstracto</div>
                <div><b>Iniciación a la algoritmia</b><br /><span className={cvStyles.description}>Semestre 5</span></div><div>Iniciación a la resolución de problemas con la algoritmia</div>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>   
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>       
        </Layout>
    );
}
