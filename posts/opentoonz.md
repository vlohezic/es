---
title: 'OpenToonz'
date: '2022-09-01'
thumbnail: "<pre>
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((((((##########(((((((((((((((((((((((((((((/.   ,((((((((((((((((((((((((((((((#########(((((((*<br />
.((((((/          .(((((((((((((((((((((((((((*      .((((((((((((((((((((((((((((,          *(((((*<br />
.((((((((////////((((((((((((((((((((((((((((((*    .(((((((((((((((((((((((((((((((///////((((((((*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((((((((((((((((/.                     /(((((((((((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((((((((((((.                               /((((((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((((((((/                                      .(((((((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((((((               ./((((((((((/,               /((((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((((            ,((((((((((((((((((((((/            /((((((((((((((((((((((((((((((((((((((((((((*<br />
.(((,          ,((((((((((((((((((((((((((((/                                                      *<br />
.((          /(((((((((((((((((((((((((((((((((                                                    *<br />
.(         .(((((((((((((((((((((((((((((((((((((                                                  *<br />
..        *(((((((((((((((((((((((((((((((((((((((                                                 *<br />
.         (((((((((((((((((((((((((((((((((((((((((         (((((((((((((         (((((((((((((((((*<br />
.        ((((((((((((((((((((((((((((((((((((((((((         (((((((((((((         (((((((((((((((((*<br />
.        ((((((((((((((((((((((((((((((((((((((((((.        (((((((((((((         (((((((((((((((((*<br />
.        ((((((((((((((((((((((((((((((((((((((((((.        (((((((((((((         (((((((((((((((((*<br />
.        /(((((((((((((((((((((((((((((((((((((((((         (((((((((((((         (((((((((((((((((*<br />
.         ((((((((((((((((((((((((((((((((((((((((         /(((((((((((((         (((((((((((((((((*<br />
.*         ((((((((((((((((((((((((((((((((((((((         .((((((((((((((         (((((((((((((((((*<br />
.(*         *((((((((((((((((((((((((((((((((((/          (((((((((((((((         (((((((((((((((((*<br />
.((/          *(((((((((((((((((((((((((((((((          ,((((((((((((((((         (((((((((((((((((*<br />
.((((.           /(((((((((((((((((((((((((            ((((((((((((((((((         (((((((((((((((((*<br />
.((((((              /((((((((((((((((/              ((((((((((((((((((((         (((((((((((((((((*<br />
.((((((((/                                        .((((((((((((((((((((((         (((((((((((((((((*<br />
.(((((((((((/                                  ,(((((((((((((((((((((((((      .((.,((((((((((((((.*<br />
.((((((((((((((((                          /(((((((((((((((((((((((((((((     /((((((((((*.    *((%*<br />
.(((((((((((((((((((((((*,..     ..,/((((((((((((((((((((((((((((((((((((   .((((((((((((((((((#%%%*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((  (((((((((((((((((#%%%%%#*<br />
.(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((*/(((((((((((((((%%%%%%%%((*<br />
.(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((,/((((((((((((#%%%%%%%%%%#(((*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((( (((((((((((%%%%%%%%%%%%%((((((*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((/.((((((((#%%%%%%%%%%%%%#(((((((((*<br />
.(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((/ (((((#%%%%%%%%%%%%%%%#(((((((((((((*<br />
.((((((((((((((((((((((((((((((((((((((((((((((((((((((((((.,((#%%%%%%%%%%%%%%%%#((((((((((((((((((*<br />
.(((((((((((((((((((((((((((((((((((((((((((((((((((((,.(%%%%%%%%%%%%%%%#((((((((((((((((((((((((((*<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
</pre>"
---

Siempre me he sentido atraído por el ámbito de la animación. He  querido aprender sobre este tema. Por consiguiente, he leído (en inglés) The ILLUSION OF LIFE: DISNEY ANIMATION de Frank Thomas y Ollie Johnston. He descubierto los pincipios de la animación :

- Estirar y encoger
- Anticipación
- Puesta en Escena
- Animación directa y Pose a pose.
- Acción complementaria y Acción superpuesta
- Acelerar y desacelerar
- Arcos
- Acción Secundaria
- Timing
- Exageración
- Dibujo Sólido
- Atractivo

Por lo tanto, he hecho videos sacar a la luz estes differentes principios. Entonces, he creado varios animaciones con el software OpenToonz

Con [Wikipédia](https://fr.wikipedia.org/wiki/OpenToonz), podemos aprender más sobre el software. OpenToonz es un programa de animación 2D de código abierto. Es una versión del software comercial Toonz desarrollado originalmente por Digital Video S.p.A, siendo desarrollado por la compañía de software Dwango.1​ Si bien Dwango ha dejado en claro que OpenToonz puede ser utilizado tanto para un uso particular como comercial sin restricción algo dejando completamente funcional el software, ha expuesto su interés en desarrollar una versión misma del software de pago "con un precio muy competitivo" con una mayor cantidad de funciones para proyectos grandes.2​ Por otra parte, la única variante comercial que existe actualmente (siendo aún de código propietario), es Toonz, que es desarrollado y comercializado por Digital Video S.p.A. Toonz ha sido utilizado por Studio Ghibli3​ y Rough Draft Studios,4​ y fue usado para producir la serie de televisión Futurama. 5​ 

Lo que me gusta especialmente con este software es su licencia que es libre. De hecho, como desarrollador, es possible de contribuir al software. Además, este software da origen a de 	clásicos como Arrietty y el mundo de los diminutos.

He creado ocho videos pero a causa de su peso, muestro solamente un video. Las imagenes de Sonic vienen de [shadow_91](https://www.spriters-resource.com/custom_edited/sonicthehedgehogmediacustoms/sheet/96811/). Para ver el video, hacer clic sobre el erizo. 


[![Animacion Sonic](https://upload.wikimedia.org/wikipedia/fr/thumb/b/ba/Project_Sonic_Logo.svg/langfr-800px-Project_Sonic_Logo.svg.png)](https://drive.google.com/file/d/1qgY5jOVPVxdpjZ7MxfO2pQ5BAkoT7x2K/view?usp=sharing "Animacion Sonic")