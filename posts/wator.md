---
title: 'WATOR'
date: '2022-06-22'
thumbnail: '<pre>
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
...................G#&@@@&###BG5Y.????........................................................???...<br />
...................B&#&&#B##&&&&&#BP5.???.....................................????...?????....5YY...<br />
...................?5B@&#G#&&###&&&&&&BPY.???....................????????????.Y55Y.?.Y5PPY..?YP5P...<br />
....................??YB&#PG#&##&&###&&&&#PY.?????...?????????....YY555YYY.YG##&&##BBBBB#@5?.??.....<br />
......................??5&&##&&&&#BBBB#&&&&@&#GPYY....YYY5PGB#&#&&&@@@@@@&&&##B5YYPBBYY5YP&.?.......<br />
........................?.#@&&##&&#BGGGB#&&&&&&@@&BBBBB#&@&&#BGGGGGB##&######5.55???P..P..#.?.......<br />
.........................?.G@&##&&BGGGPPG#GBGBGGGPPGBBGPYY..?????????Y&#B###B?.55??YBB55G&BY..??....<br />
..........................??G@##&&&&&#PPPPPPP5Y5PG#GY.????.....???.YP##GGBBBBGGGPPG########&##GY??..<br />
............................?#&&&&&&&&BPPPPP5555B#.???..??????.Y5G####BBGBGPPPGB###BBBBBBBBB###&#5?.<br />
............................?G@&&&&&&&&GPPPP..PPG#PY.....Y5PGB#####BBBB#BB#B#BPGBBBBBBBBBBBBBBGB&@P?<br />
..........................?.G&&&&&&&#BBGP5PPPPPPPB&&######&#BBBBBBB#BGB#B##&&BBB##BBBB#BBBBG5555PG@G<br />
........................?.P&@##&&&&&##&BPPPPPPPPPPGGGGBBBBGGPPBGP55GB#&#G#@#&&##&#BGPB#BBBBBBGBGY5&@<br />
......................?.P&@#BB#&&#GGGGBBBBPPPPPPPPPPBBBBBGP555BG5Y5PG&&Y?Y#?!.B5P###&&&&&###B###B#@G<br />
....................?.PBPYG&&&&&###BPPPPGBPPP55PPPPPPPPPPPY.5PGBBBB#&G.?.?YBY~^..!..5YY5PPGBBB@#GPY?<br />
..................?.PBP.???.&&&&##&&#PPPP5PPPPPPPPPPPP5555PPBBBB###BY??...?.PG5.7?..?????????GP.???.<br />
................??5BP.??????B&&&&&&&&GPPBB#BBGGGGB#####BGGPPB###BPY??.......??.YPPGGPP55555YB#??....<br />
...............?.B#.????..5B&&&&&&&&&&##&&&&&&&&&BGGGGPPGGB#&&P.???............????..YYYY55PGGBY?...<br />
..............?Y&@&##BB##&&&####&&#####&&&&&&#&@&##BPP5.YYYY5B#G.?..................???......5#Y?...<br />
.............?5&&&&&&&&##BBBBBBB##&&##BGGP5Y.P#&###BBBBG5555PGB&#.?..............???.5GGGPPPP5.?....<br />
............?Y&&&&##BBGGGGGBB##&#BG5Y..????YB&&#&&&&&&&BP555PGGB@P?........?????.YPGG5Y???????......<br />
.............#&###BBGPPPPB#&#PY?..???...?.G&&&&&&&&&&&&#BP5PPGPP&P????????.YY5GB&BY.???.............<br />
...........?P@####BGPPGB#&G?~:::~7??????P&&&&&&&&&&&##BGPB##BBB#BY.YY5PPGB#BBGP5G#P?................<br />
............#&&#BGPPPB&#5!^:::^^~~7Y55YG@&&&&&&&&#BPY5###&&&#&BGGPPGPPPBB#&&##G5Y5@5?...............<br />
..........?Y@&#&#G5PB&5~::^!.5PPPPPPP5G@&&&##&&BGP5PGB&&#BG5Y.???????????..Y5GB#BG#Y?...............<br />
...??????.?5@&BBGPPG&.::~?5PY...??????.B&&&&&&&###BGGP5Y.????..............???..YY.?................<br />
...Y5555Y.?5@&&#BGPB#^:7G5Y5555555.....?.YYYYYY...??????........................??..................<br />
.?G#GPP5PGG5&&&&#GPG#^.BPP5Y..5PG#G?....???????.....................................................<br />
.YPGPPP.?.YG#@&&&#GPGY#BY????.PGG#P?................................................................<br />
BPYYPPY.Y5PP5G&@&&#BG&@G5PP5Y.5B#BP?................................................................<br />
YYYBG555YYY555PPGGGGGGGGPPY5PBGGB.?.................................................................<br />
??.P55.?????YGGGGP555YYY5Y???.YP.?..................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />


</pre>'
---

TIPE es un prueba oral para ingresar a una escuela de ingeniería. Trabajé con Gabriel Giovannelli. Trabjamos sobre diferentes modelizaciones para describir la evolución de diferentes poblacionces. El estudio se limita a un modelo presa-predador.

Una primera modelización es encontrado por Malthus. En las ecuaciones  de esta modelización ninguna parámetro permite limitar las soluciones, por eso el número de presas aumenta sin interrupción.

Alfred Lokta y Vito Volterra trabajaron cada uno por su lado sobre un modelo para describir la evolucion de las cantidades de piel de liebres y linces vendidas por los tramperos a la Hudson's Bay Company. [Esta modelización](https://fr.wikipedia.org/wiki/%C3%89quations_de_pr%C3%A9dation_de_Lotka-Volterra) describe de manera más realista el comportamiento de presas y predatores. Con esta modelización, podemos notar un fenómeno de periodicidad en la evolución de presas y predatores. 

Un otro enfoque es possible. La dinámica de poblaciones es modelizada con un [autómata celular](https://www.futura-sciences.com/tech/definitions/informatique-automate-cellulaire-8909/).

En 1984, en la revista Scientific American, A; K. Dewdney  crea un planeta. Tiburones y peces viven en este planeta El Wator es un mundo en forma de Toro. El tiempo se llama chronons. La modelización es discreta. Para cada chronon, eligimos una casilla aleatoria. Si es lleno de un animal, el pez o el tiburón move según un comportamiento exacto. La modelización del Wa-Tor es explicada [aquí](https://en.wikipedia.org/wiki/Wa-Tor).

En el marco del TIPE, nuestra problemática era : **¿Qué influencia puede tener el ser humano en un sistema presa-predador?**

Hay tres estudios :
- [un primero sobre la pesca](https://drive.google.com/file/d/1qQMw_Q47wkwNhOkhAszcAHGKm4CKnt0V/view?usp=sharing)
- [un segundo sobre la urbanización](https://drive.google.com/file/d/1Z533_pt8l1Cbbb6ZNxr6D1tjKWFIlqxx/view?usp=sharing) 
- [une último sobre los pasos de fauna](https://drive.google.com/file/d/1oJ53F3hmRxCaMpgPPSuxWxvi4tXtepAI/view?usp=sharing) 


La presentación en diapositivas es disponible [ici](https://docs.google.com/presentation/d/1W7GSCfO0eWrtKu3Ev5j9tJtCFOhej-oDkCf7WvcZar0/edit?usp=sharing).