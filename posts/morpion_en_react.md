---
title: 'Cinco en línea en React'
date: '2022-06-30'
thumbnail: "<pre>....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
    ........^^:::::::::::::::::::::::::~^:::::::::::::::::::::::::~!7777777777777777777777777!..........<br />
      ........~:                         ~:                         ~Y5555555555555555555555555J..........<br />
      ........~:                         ~:                         ~YYYYYYYYYYYYYYYYYYYYYYYYYYJ..........<br />
      ........~:                         ~:                         ~YYYYYYYYYYYYYYY5YYYYYYYYYYJ..........<br />
      ........~:      :5BP:   7BG!       ~:                         ~YYYYYYYJ^:^J5YY7::7YYYYYYYJ..........<br />
      ........~:       :5@&!.5@#~        ~:                         ~YYYYYYYYJ^ .?Y^ .?YYYYYYYYJ..........<br />
      ........~:         !#@#@J.         ~:                         ~YYYYYYYYY57. . ~Y5YYYYYYYYJ..........<br />
      ........~:          5@@#^          ~:                         ~YYYYYYYYY5Y~  .?5YYYYYYYYYJ..........<br />
      ........~:        ^B@57&@J         ~:                         ~YYYYYYYY5?: ~7. ~Y5YYYYYYYJ..........<br />
      ........~:      .Y@@7  :G@#~       ~:                         ~YYYYYYYY~  7Y5J: .?YYYYYYYJ..........<br />
      ........~:      ~YJ^     7YJ:      ~:                         ~YYYYYYY?~!J5YYYY7~!JYYYYYYJ..........<br />
      ........~:                         ~:                         ~YYYYYYY5555YYYYY555YYYYYY5J..........<br />
      ........~^.:::::::::::::::::::::::.!^::::::::::::::::::::::::.!JYJJJJJJJJJJJJJJJJJJJJJJJY?..........<br />
      ........~^.........................~YYYYYYYYYYYYYYYYYYYYYYYYYY?:.........................~:.........<br />
      ........~:                         ~YYYYYYYYYYYYYYYYYYYYYYYYYYJ.                         ~:.........<br />
      ........~:                         ~YYYYYYYY55YYYYYY55YYYYYYYYJ.                         ~:.........<br />
      ........~:         ^!7?7~:         ~YYYYYYYJ77?5YY5Y77?YYYYYYYJ.         ^!7?7~:         ~:.........<br />
      ........~:      .J#&BPPG&@G!       ~YYYYYYYY!  !Y5?: ^JYYYYYYYJ.      .J#&BPPG&@G!       ~:.........<br />
      ........~:     .G@#^    .J@@?      ~YYYYYYYY5J: ^! .7YYYYYYYYYJ.     .G@#^    .J@@J      ~:.........<br />
      ........~:     !@@!       G@#.     ~YYYYYYYYY55~  .J5YYYYYYYYYJ.     !@@!       P@#.     ~:.........<br />
      ........~:     !@@?       B@B      ~YYYYYYYYYY7..: ^J5YYYYYYYYJ.     ~@@?       B@B      ~:.........<br />
      ........~:      Y@@Y^..:!G@#^      ~YYYYYYY5J^ :JY~ .7YYYYYYYYJ.      Y@@Y^..:!G@#^      ~:.........<br />
      ........~:       ~5###B##G7.       ~YYYYYYY?: ~Y5Y5?..~YYYYYYYJ.       ~5B##B##G7.       ~:.........<br />
      ........~:          .::.           ~YYYYYYYYJYYYYYY5YYJYYYYYYYJ.          .:::           ~:.........<br />
      ........~:                         ~Y5555555555555555555555555J.                         ~:.........<br />
      ........~777777777777777777777777777!!!!!!!!!!!!!!!!!!!!!!!!!!7^:::::::::::::::::::::::::!:.........<br />
      ........~Y5555555555555555555555555J.                         ~:                         ~:.........<br />
      ........~YYYYYYYYYYYYYYYYYYYYYYYYYYJ.                         ~:                         ~:.........<br />
      ........~YYYYYYYYYYYYYYY5YYYYYYYYYYJ.                         ~:                         ~:.........<br />
      ........~YYYYYYYJ^.^J5YY7::7YYYYYYYJ.                         ~:       .7PBBBBGY!        ~:.........<br />
      ........~YYYYYYYYJ^ .?Y^ .?YYYYYYYYJ.                         ~:      7&@P7^^~J&@G:      ~:.........<br />
      ........~YYYYYYYYY5?. . !Y5YYYYYYYYJ.                         ~:     ^@@J      :#@G      ~:.........<br />
      ........~YYYYYYYYY5Y~  .?5YYYYYYYYYJ.                         ~:     7@@!       P@&.     ~:.........<br />
      ........~YYYYYYYY5?: ~7. ~Y5YYYYYYYJ.                         ~:     :#@G:     !@@Y      ~:.........<br />
      ........~YYYYYYYY~  755J: .?YYYYYYYJ.                         ~:      ^P@&PYJYB@#?       ~:.........<br />
      ........~YYYYYYY?!!J5YYYY7!!JYYYYYYJ.                         ~:        :!JYYY?~.        ~:.........<br />
      ........~YYYYYYY5555YYYYY555YYYYYY5J.                         ~:                         ~:.........<br />
      ........~JJJJJJJJJJJJJJJJJJJJJJJJJJ?^.:::::::::::::::::::::::.~^.:::::::::::::::::::::::.~:.........<br />
      ....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br />
....................................................................................................<br /> </pre>"
---

Para desarollar mis conocimientos en [React](https://fr.reactjs.org/), he creado un cinco en línea. React es un framework Javascript para crear interfaces de usuario. He mirado [este tutorial](https://fr.reactjs.org/tutorial/tutorial.html). Además, hay otras funciones a añadir. He añadido : 
- Muestra la ubicación para cada movimiento en el formato (columna, fila) en la lista del historial de movimientos. ✅
- Convierte en negrita el elemento actualmente seleccionado en la lista de movimientos. ✅
- Reescribe el Board para usar 2 ciclos para hacer los cuadrados en vez de escribirlos a mano. ✅
- Agrega un botón de switch que te permita ordenar los movimientos en orden ascendente o descendente. ✅
- Cuando alguien gana, resalta los 3 cuadrados que hicieron que gane. ✅
- Cuando nadie gana, muestra un mensaje acerca de que el resultado es un empate.✅

En el cinco en línea, hay un historial para volver a un estado passado. El desarrollo de esta función es una manera de poner en práctica los conocimientos del curso de programación funcional.

El cinco en línea es disponible aquí : [https://vlohezic.gitlab.io/react_morpion/](https://vlohezic.gitlab.io/react_morpion/).    