---
title: 'Grunt'
date: '2022-06-26'
thumbnail: "<pre>
....................................................................................................<br />
.....................????????.............??...?.YPGG....??..........????????.......................<br />
.................??..YYYYYYY...??.......?.5GP?.5GBBBP???..Y.....???..YYYYYYY...??...................<br />
...............?.YPGGBBGGGGBBBGPY.??...?YGBBG.PBBBBBGY5PGBG...??.5PGBBBGGGGBBGG5Y.?.................<br />
...............YGBBBB5.????7?YPBBBPY???.GBBBBGBBBBBBBBBBBBY??.YPBBB5.?7????.5BBBBGY?................<br />
...............YY55PGBBBGPGP5?!7YGBBPY?PBBBBBBBBBBBBBBBBBG??YGBBG.7!.5PGPGBBBGP5YY..................<br />
...............?????.YGBB.?.PBG.!!5BBBGBBBBBBBBBBBBBBBBBBBGGBBBY!!YGB5.?YBBPY.????..................<br />
....................??YBB5777YBBP5GBBGGP5Y..???7777???..Y5PGBBBP5GBG.777PBB.??......................<br />
.................??...5BBG?77.GBBBP5?7!!!!!!!!!!!!!!!!!!!!!!7.5PBBBP?77.BBBY...??...................<br />
................?YPGBBBBBBBGGBBGY?77!!!!!!!!!!!!!!!!!!!!!!!!!!77?5BBBGGBBBBBBBG5.?..................<br />
...............?5BBP.??7??.Y5PGBGPY?7!!!!!!!!!!!!!!!!!!!!!!!!7.5PGBGP5Y.??7??.PBBY?.................<br />
...............?PBB7!!!!!!!!!!!7.5GBP.7!!!!!!!!!!!!!!!!!!!!7YGBG5.7!!!!!!!!!!~?BBP?.................<br />
...............?5BB5!7!!!!!!!!!!!!!?YPP.!!!!!!!!!!!!!!!!!!.GPY7!!!!!!!!!!!!!77PBBY?.................<br />
................?5BBP5!!!!!!!!!!!!!!!!?YY7!!!!!!!!!!!!!!7YY7!!!!!!!!!!!!!!!!PGBGY?..................<br />
.................?5BBG7!!!!!!!!!!!!!!!!!!7!!!!!!!!!!!!!!7!!!!!!!!!!!!!!!!!!?BBBY?...................<br />
.................?.BBB7!7?...??77!!!!!!!!!!!7?77!!77?7!!!!!!!!!!!77??..??!!.BBG.....................<br />
.................?5BBG7!!YBBBBBGP5.?7!!!!!!!!?Y5PP5Y?!!!!!!!77?.5GBBBBBG.!!?BBBY?...................<br />
...............?.YGBBY!!!PGBBG~75BBG5.7!!!!!!!!777!!!!!!!!!7.5BBGY!!BBGB5!!75BBGY.?.................<br />
...............5GBGGP7!!!.?PBG:...:75BBP.7!!!!!!!!!!!!!!!!7.PBBY!...^BB5.?!!!?PGBBGY................<br />
.............?5BBG?.?7!!!!!?GBY:....^YBBP5Y.Y?!!!!!!.Y.Y5GBB.:....^5BG7!!!!!7?..GBB5?...............<br />
.............?PBBP777!!!!!!!?5GPY?777.GBBBGGY!!!!!!!75GBBBBP?777?YGG57!!!!!!!777PBBP?...............<br />
.............?YGBB5?777!!!!!!7?.Y5PP55.5B5!!!!!!!!!!!!!7PBY.5PPP5Y.7!!!!!!!777.PBBG.................<br />
..............?.5GBBP5.?7777!!!!7777777.G7!!!!!!!!!!!!!!?G?7777777!!!!7777?.5GBBG5..................<br />
................?.Y5GBBGPY?77!!!!!!!!77.7!!!!!!!!!!!!!!!!7?77!!!!!!!!77?YPGBBG5Y.?..................<br />
..................??..YPBBP?77!!!!!!77!!!!!!!!!!!!!!!!!!!!!!7!!!!!!!77.GBBPY.???....................<br />
......................??YBBP?777!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!777?GBGY??........................<br />
........................?GBB?7?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7?7.BBP?..........................<br />
...................??...?GBB?7?!!!!!!!!?7!!!!!!!!!!!!!!!!??!!!!!!!7?7.BBP?...??.....................<br />
.................?.55?..?GBG?77!!!!!!!?P!!!!!!!!!!!!!!!!!7P7!!!!!!!?7.BB5?..?P5.?...................<br />
................?.GBBY?..GBG?77!!!!!!?B.!!!!!!!!!!!!!!!!!!5G7!!!!!!77?BBG..?5BBP.?..................<br />
...............?.GBGBB5?.BBP7?7!!!!!?GG!!!!!!!!!!!!!!!!!!!7BP7!!!!!77?GBG?.5BBGBG...................<br />
...............?PBG~?BBP5BBP77!!!!7?PB.!!!!!!!!!!!!!!!!!!!!YB577!!!!77PBB5GBB77BB5?.................<br />
................GBP^ ~5B#BB5?7!!!775B5!!!!!!!!!!!!!!!!!!!!!!PBY77!!!7?PBB#BY^ ~GBG?.................<br />
................GBG~:  ^?5GBBGGPPPPBG!!!!!!!!!!!!!!!!!!!!!!!7GBPPPPGGBGPY7:  :!GBP?.................<br />
...............?5BB7^:    .:~!7?YBBB5YYYYYYYYYYYYYYYYYYYYYYYYPBBG.?7!^:.    :^.BBY?.................<br />
.................GBG!^^:       ~PBGP55555555555555555555555555PGB5^      .:^^7GBP...................<br />
................?.GBG.!^^::.. !BB.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!7YBB^ ..:^^^!YBBP....................<br />
.................?.5GBG5.7~~^^.BB7!7GGGGGGGB5!!!!!!!7PGGGGGGGP!!?BB7^^~!7.PBBG5.....................<br />
...................?.YPGBBGPP5PBB5!!YBBBBBBG?!!!!!!!!.GBBBBBG.!!PBB55PGGBBG5Y.?.....................<br />
.....................??..PBBB555GBY!!?5PGPY7!!!!!!!!!!?5PGPY7!!5BG55PBBB5..??.......................<br />
........................??YGBG.7?GB57!!!7!!!!!!!!!!!!!!!!7!!!?PBP77YGBGY?...........................<br />
..........................?.5GBB55BBGY?777!!!!!!!!!!!!!7777?5GBG5PBBG5.?............................<br />
............................?.Y5GBBBBBG5.?77777777777777?.5GBBBBGG5Y.?..............................<br />
..............................??...YY5GBBGP5YY......YY5PGBBP5YY...??................................<br />
..................................????..5PGGBBBBBBBBBBGGPY.?????....................................<br />
....................................................................................................<br />
....................................................................................................<br />
</pre>"
---

En el marco de un proyecto OpenSource, he descubierto el uso de Grunt. Grunt es una herramienta para la creación de tareas automatizadas con el lenguaje JavaScript. 

Este artículo es un tutorial para acordarse de Grunt.
Primero, es necesario que descargue [el archivo siguiente](https://drive.google.com/file/d/1CZKWkA4A-QqlOE6uO7onVcdlI1pjtrVP/view?usp=sharing). Este archivo corresponde a una versión de demostración de un sitio web proponido por [documentación de Materialize](https://materializecss.com/getting-started.html). He suprimido los ficheros css et js minimizados.


Para instalar grunt sobre su ordenador : 

```
npm install -g grunt-cli
```

Empezamos para ir en el directorio del proyecto.
inicializamos el proyecto con el comando : 
```
npm init
```


Instalamos el módulo grunt :

```
npm install grunt --save-dev
```

Creamos un Gruntfile.js con el código siguiente : 
```
module.exports = function(grunt) {
    
}
```

Después, vamos a usar algunos complementos en [este enlace](https://gruntjs.com/plugins). Los complementos empezandos por contrib son los complementos creando por el grupo de Grunt. 

Empezamos por concatenar nuestros ficheros js. Vamos a usar el complemento [contrib-concat](https://www.npmjs.com/package/grunt-contrib-concat). Instalamos :
```
npm install grunt-contrib-concat --save-dev
```

Encargamos la tarea : 
```
module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
          options: {
            separator: ';',
          },
          dist: {
            src: ['js/init.js', 'js/materialize.js'],
            dest: 'dist/js/built.js',
          },
        },
      });

    grunt.loadNpmTasks('grunt-contrib-concat');
}
```

grunt.initconfig initialise l'ensemble des tâches.

Después, minizamos el fichero javascript creado. Instalamos el módulo [uglify](https://www.npmjs.com/package/grunt-contrib-uglify) :

```
npm install grunt-contrib-uglify --save-dev
```

Encargamos la tarea : 

```
module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
          options: {
            separator: ';',
          },
          dist: {
            src: ['js/init.js', 'js/materialize.js'],
            dest: 'dist/js/min.js',
          },
        }, 
      });
    
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
}
```

Añadimos la minimización : 

```
module.exports = function (grunt) {
  grunt.initConfig({
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['js/init.js', 'js/materialize.js'],
        dest: 'dist/js/min.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'js/min.js': ['js/min.js']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
}
```

Creamos una nueva tarea que es una combinación de las dos precedentes tareas :

```
  grunt.registerTask('default', ['concat', 'uglify']);
```

Luego, podemos encargar todas las tareas necesarias a nuestro proyecto en una línea con el módulo [load-grunt-config](https://www.npmjs.com/package/load-grunt-tasks).

Instalamos el módulo :
```
npm install load-grunt-tasks --save-dev
```

Al principio del fichero, añadimos la línea siguiente : 
```
require('load-grunt-tasks')(grunt);
```

Ahora, vamos a minimizar los ficheros css. Por consiguiente, necesitamos del [módulo](https://www.npmjs.com/package/grunt-contrib-cssmin) :
```
npm install grunt-contrib-cssmin --save-dev
```

Para combinar dos ficheros css en uno :
```
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['js/init.js', 'js/materialize.js'],
        dest: 'dist/js/min.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'dist/js/min.js': ['dist/js/min.js']
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'dist/css/min.css': ['css/materialize.css', 'css/style.css']
        }
      }
    }
  });

  grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
}
```

Después, minizamos el fichero css : 
```
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['js/init.js', 'js/materialize.js'],
        dest: 'dist/js/min.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'dist/js/min.js': ['dist/js/min.js']
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'dist/css/min.css': ['css/materialize.css', 'css/style.css']
        },
        min: {
          expand: true,
          cwd: 'dist/css',
          src: ['dist/css/*.css'],
          dest: 'dist/css',
          ext: '.min.css'
        }
      }
    }
  });

  grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
}

```
Vamos a copiar el fichier html y las imagenes en el directorio dist. Necesitamos el módulo [grunt-contrib-copy](https://www.npmjs.com/package/grunt-contrib-copy):

```
npm install grunt-contrib-copy --save-dev
```

Para copiar el fichero html, modificamos el Gruntfile.js : 
```
    copy: {
      index: {
        src: 'index.html',
        dest: 'dist/index.html',
        options: {
          process: function (content, srcpath) {
            return content
              .replace('<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>', '')
              .replace('<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>', '<link href="css/min.css" type="text/css" rel="stylesheet" media="screen,projection"/>')
              .replace('<script src="js/materialize.js"></script>', '')
              .replace('<script src="js/init.js"></script>', '<script src="js/min.js"></script>')
              ;
          },
        },
      },
      background1: {
        src: 'background1.jpg',
        dest: 'dist/background1.jpg',
      },
      background2: {
        src: 'background2.jpg',
        dest: 'dist/background2.jpg',
      },
      background3: {
        src: 'background3.jpg',
        dest: 'dist/background3.jpg',
      }
    },
```

Podemos notar que haya más que una copia para el fichero html. De hecho, reemplazamos las líneas para importar CSS y JS con líneas para importar su versión minimizada.

Luego, instalamos el módulo [grunt-contrib-connect](https://www.npmjs.com/package/grunt-contrib-connect) para 	poner en marcha un servidor.

```
npm install grunt-contrib-connect --save-dev
```

Modificamos el Gruntfile.js de la manera siguiente : 

```
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  // Update this variable if you don't want or can't serve on localhost
  var hostname = 'localhost';

  var PORT = {
    PROD: 9001,
    DEV: 9901,
    TEST: 9991
  };

  var getConnectConfig = function (base, port, host, open) {
    return {
      options: {
        port: port,
        hostname: host,
        base: base,
        open: open, 
        keepalive: true //run a long running server
      }
    };
  };

  grunt.initConfig({
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['js/init.js', 'js/materialize.js'],
        dest: 'dist/js/min.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'dist/js/min.js': ['dist/js/min.js']
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'dist/css/min.css': ['css/materialize.css', 'css/style.css']
        },
        min: {
          expand: true,
          cwd: 'dist/css',
          src: ['dist/css/*.css'],
          dest: 'dist/css',
          ext: '.min.css'
        }
      }
    },
    copy: {
      index: {
        src: 'index.html',
        dest: 'dist/index.html',
        options: {
          process: function (content, srcpath) {
            return content
            .replace('<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>','')
            .replace('<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>','<link href="css/min.css" type="text/css" rel="stylesheet" media="screen,projection"/>')
            .replace('<script src="js/materialize.js"></script>','')
            .replace('<script src="js/init.js"></script>', '<script src="js/min.js"></script>')
            ;
          },
        },
      },
      background1: {
        src: 'background1.jpg',
        dest: 'dist/background1.jpg',
      },
      background2: {
        src: 'background2.jpg',
        dest: 'dist/background2.jpg',
      },
      background3: {
        src: 'background3.jpg',
        dest: 'dist/background3.jpg',
      }
    },
    connect: {
      prod: getConnectConfig('dist', PORT.PROD, hostname, true)
    }
  });

  grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'copy', 'connect']);
}
```

Para que Grunt 	cumpli su tareas de manera automática cuando realizamos modificaciones sobre nuestro css o js, podemos usar este módulo [grunt-contrib-watch](https://www.npmjs.com/package/grunt-contrib-watch). 

```
npm install grunt-contrib-watch --save-dev
```

En files, cuando al principio de la ruta hay un signo de exclamación significa que debe le ignorar. 

```
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  // Update this variable if you don't want or can't serve on localhost
  var hostname = 'localhost';

  var PORT = {
    PROD: 9001,
    DEV: 9901,
    TEST: 9991
  };

  var getConnectConfig = function (base, port, host, open) {
    return {
      options: {
        useAvailablePort: true,
        hostname: host,
        base: base,
        open: open,
        livereload: true
      }
    };
  };

  grunt.initConfig({
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['js/init.js', 'js/materialize.js'],
        dest: 'dist/js/min.js',
      },
    },
    uglify: {
      dist: {
        files: {
          'dist/js/min.js': ['dist/js/min.js']
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'dist/css/min.css': ['css/materialize.css', 'css/style.css']
        },
        min: {
          expand: true,
          cwd: 'dist/css',
          src: ['dist/css/*.css'],
          dest: 'dist/css',
          ext: '.min.css'
        }
      }
    },
    copy: {
      index: {
        src: 'index.html',
        dest: 'dist/index.html',
        options: {
          process: function (content, srcpath) {
            return content
              .replace('<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>', '')
              .replace('<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>', '<link href="css/min.css" type="text/css" rel="stylesheet" media="screen,projection"/>')
              .replace('<script src="js/materialize.js"></script>', '')
              .replace('<script src="js/init.js"></script>', '<script src="js/min.js"></script>')
              ;
          },
        },
      },
      background1: {
        src: 'background1.jpg',
        dest: 'dist/background1.jpg',
      },
      background2: {
        src: 'background2.jpg',
        dest: 'dist/background2.jpg',
      },
      background3: {
        src: 'background3.jpg',
        dest: 'dist/background3.jpg',
      }
    },
    connect: {
      prod: getConnectConfig('dist', PORT.PROD, hostname, true)
    },
    watch: {
      js: {
        files: ['js/*.js', 'css/*.css'],
        tasks: ['default'],
        options: {
          spawn: false,
        },
      }
    },

  });

  grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'copy', 'connect', 'watch']);
}
```

Suprimos keepalive porque evitar hacer tareas en paralelo. 

Para conocer más sobre este tema, [Gulp](https://gulpjs.com/) es una otra herramienta para automatizar las tareas. 